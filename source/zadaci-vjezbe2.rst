Zadaci za vježbu
================

.. admonition:: Priprema za zadatke 1 i 2

	Sljedeće probleme riješite čitanjem tekstualne datoteke pojedinačno (liniju po liniju), izvršavajući operacije u svakom retku pomoću odgovarajućih struktura podataka. 
	
	Prilikom implementacije obratite pažnju da vam se program izvodi u što kraćem vremenu.
	
	U nastavku vam je zadan predložak koji možete koristiti za stvaranje i čitanje tekstualne datoteke.
	
.. code-block:: cpp
	
	#include <iostream>
	#include <fstream>
	#include <string>
	
	//ne zaboravite include za srukturu podataka koju koristitie

	using namespace std;

	int main() {
		
		//-------------------------------------
		// Stvaranje datoteke sa kojom se radi
		//-------------------------------------
		
		fstream file_name;
		file_name.open("test.txt", ios::out);  
		// učitavanje datoteke za upis sadržaja

		if (file_name.is_open()) //provjera ako je datoteka učitana
		{
			//zadajete interval do gdje ide for
			//petlja ovisno o tome koliko linija u 
			//.txt datoteci želite imati, u 
			//daotetku se pohranjuju cjeli brojeviod 0 do RANGE
			for (int i = 0; i < RANGE; i++)
			{
				file_name << i << '\n';   //unos vrijednosti
			}
			file_name.close();
		}
		
		//STRUKTURA PODATAKA definicija strukture 
		//podataka koja se koristi (stack, queue, list ...)
		
		file_name.open("test.txt", ios::in); 
		//učitavanje datoteke iz koje se čita sadržaj
		
		//-------------------------------------
		// Čitanje datoteke i rad sa linijama
		//-------------------------------------
		
		if (file_name.is_open()) {
			string tp; //deklariranje varijablu tp 
			//tipa string za pohranu učitanog sadržaja retka iz datoteke
			
			cout << "File contents: \n";

			while (getline(file_name, tp)) { //čitanje sadržaja datoteke i pohrana u string
				
				
				//-------------------------------------
				// kôd za rad sa linijama
				//-------------------------------------
				
				
			}
			file_name.close(); 		
		}
	}

.. admonition:: Zadatak 1

	Učitajte redke jedan po jedan, a zatim ih ispišite obrnutim redoslijedom, tako da se najprije ispisuje posljednji učitani redak, zatim predzadnji učitani redak i tako dalje.

.. admonition:: Zadatak 2

	Pročitajte prvih 50 redaka iz tekstualne datoteke, a zatim ih ispišite obrnutim redoslijedom. Pročitajte sljedećih 50 redaka i zatim ih isto ispišite obrnutim redoslijedom. Učitavajte retke sve dok nema preostalih redaka za čitanje, a zatim preostali redovi trebaju biti ispisani obrnutim redoslijedom.
	
	Drugim riječima, vaš će ispis početi s 50. redom, zatim 49., zatim 48., i tako dalje sve do prvog reda. Zatim sljedi 100. redak, slijedi 99., i tako sve do 51. retka. I tako dalje. Vaš kôd nikada ne bi trebao pohraniti više od 50 redaka u bilo kojem trenutku.
	
	Imajte na umu da prilikom posljednjeg učitavanja može biti manje od 50 redaka koje se isto trebaju ispisati obrnutim redosljedom.

.. admonition:: Priprema za zadatak 3
	
	U matematici je faktorijel pozitivnog cijelog broja **n**, označen s **n!**, umnožak svih pozitivnih cjelobrojnih brojeva manji od ili jednak **n**:

	- n! = n × (n - 1) × (n - 2) × (n - 3) × ... × 3 × 2 × 1. 

	Na primjer:

	- 5! = 5 × 4 × 3 × 2 × 1 = 120. 

	Vrijednost 0! je 1

	.. admonition:: Zadatak 3

		Definirajte funkciju **faktorijel(n)** koja vraća izračunati faktorijel (n!) pomoću rekurzije, za prosljeđeni parametar n. 
		
Predaja rješenja
----------------

Da bi vam se priznao dolazak potrebno je rješene zadatke predati na Merlinu, **rok za predaju je 13.03.2020 23:59**. 

Rješenja predajete u obliku komprimirane datoteke (Ime_Prezime.rar, .zip i sl.) koja sadrži .cpp datoteke, .cpp datoteke imenujte na način (Ime_Prezime_zd_brojZadatka.cpp). **Molim vas da obavezno datoteke imenujete na zadani način**.

Nije dopušteno predati identična riješenja nekoga od kolega.

Za sva pitanja stojim vam na raspolaganju na mailu: milan.petrovic@uniri.hr

Želim vam puno sreće i uspjeha u rješavanju zadataka.