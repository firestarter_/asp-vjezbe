
Binarna stabla
==============


Stabla: za razliku od polja, povezanih lista, stoga i redova, koji su linearne strukture podataka, stabla su hijerarhijske strukture podataka.


Matematički gledano, binarno stablo je povezan, neizmjeren, konačni graf bez ciklusa i bez stupnja vrha većeg od tri.Z

Binarno stablo: Stablo čiji elementi imaju najviše 2  čvora djece naziva se binarno stablo. Budući da svaki element u binarnom stablu može imati samo 2 čvora djece, obično ih imenujemo lijevo i desno dijete.

Binarna stabla su ukorijenjena, najčešće u čvoru **r**. Poseban čvor **r** (stupnja najviše dva) naziva se korijenom stabla. Prilikom ilustracije binarnog stabla, prikaz kreće od čvora **r** koji se prikazuje  na vrhu a ostali čvorovi ispod njega, čime je **r** korijen stabla.

.. code-block:: cpp

      stablo
      ----
       r    <-- korijen
     /   \
    f      k  
  /   \      \
 a     h      z    <-- listovi 
 

Čvorove razlikujemo na roditelje i djecu. 
Ako čvor **a** ispod sebe sadrži čvorove **b** i **c**, čvor **a** nazivamo roditeljem od **b** i **c**, sukladno tome **b** i **c** su djeca čvora **a**.

.. code-block:: cpp

      stablo
      ----
     /
    a     <-- čvor roditelj
   /  \
  b    c     <-- djeca čvora a



U radu sa binarnim stablima koristi se terminologija:

    - **Dubina čvora** Dubina čvora **u**, u binarnom stablu je duljina puta od **u** do korijena stabla **r**.
    - **Podstablo čvora** Podstablo čvora **u**, je binarno stablo koje sadrži sve čvorove (uključujući djecu) ukorijenje u čvoru **u**.
    - **Visina stabla** Visina stabla, je duljina najduljeg puta od **r** (korijena) do njegovog najudaljenijeg djeteta.
    -  **Visina čvora** Visina čvora **u**, je duljina najduljeg puta od **u** do njegovog najudaljenijeg djeteta.
    -  **List** Čvor nazivamo list ako on nema djece.


Predstavljanje binarnog stabla u C ++: Stablo je predstavljeno pokazivačem na najviši čvor na stablu. Ako je stablo prazno, vrijednost korijena je NULL.

Čvor unutar binarnog stabla sadrži sljedeće dijelov:
    - Podatke zapisane u tom čvoru
    - Pokazivač na lijevo dijete
    - Pokazivač na desno dijete


Putovanje kroz stablo
---------------------

Za razliku od linearnih struktura podataka. Kroz koje možemo prolaziti na samo jedan logički način, putovanja kroz stabla se mogu kretati na različite načine.

.. code-block:: cpp

    /* Binarno stablo sadrzi, pdoatke u čvoru, pokazivac na ljevi cvor djeteta i 
    pokazivac na desni cvor djeteta */
    struct Node
    {
        int data;
        struct Node* left, *right;
        Node(int data)
        {
            this->data = data;
            left = right = NULL;
        }
    };


**Preorder** prolazak kroz stablo na principu čvor -> ljevi djete čvor -> desni djete čvor

.. code-block:: cpp

    /* Za dano binarno stablo, ispis cvorova u redosljedu 
    cvor -> ljevi djete cvor -> desni djete cvor*/
    void printPreorder(struct Node* node)
    {
        if (node == NULL)
            return;
      
        /* prvo pristup cvour */
        cout << node->data << " ";
      
        /* onda pristup ljevom podstablu */
        printPreorder(node->left);
      
        /* i na kraju pristup desnom podstablu */
        printPreorder(node->right);
    }

**Inorder** prolazak kroz stablo na principu ljevi djete čvor -> čvor -> desni djete čvor

.. code-block:: cpp


    /* Za dano binarno stablo, ispis cvorova u redosljedu 
    ljevi djete cvor -> cvor -> desni djete cvor*/
    void printInorder(struct Node* node)
    { 
        if (node == NULL)
            return;
      
        /* prvo pristup ljevom cvoru djetetu */
        printInorder(node->left);
      
        /* onda ispis informacija u cvoru */
        cout << node->data << " ";
      
        /* nakon toga pristup desnom djetetu */
        printInorder(node->right);
    }
      


**Postorder** prolazak kroz stablo na principu ljevi djete čvor -> desni djete čvor -> čvor

.. code-block:: cpp

    /* Za dano binarno stablo, ispis cvorova u redosljedu 
    ljevi djete cvor -> desni djete cvor -> cvor*/
    void printPostorder(struct Node* node)
    {
        if (node == NULL)
            return;
      
        // fprvo pristup ljevom podstablu
        printPostorder(node->left);
      
        // nakon toga pristup desnom podstablu
        printPostorder(node->right);
      
        // na kraju pristup informaciji u cvoru
        cout << node->data << " ";
    } 


Usporedimo rad ovih funkcija

.. code-block:: cpp

    int main()
    {
        struct Node *root = new Node(1);
        root-> = new Node(2);
        root-> = new Node(3);
        root->left-> = new Node(4);
        root->left->right = new Node(5);
      
        cout << "\nPreorder prolazak kroz stablo: \n";
        printPreorder(root);
      
        cout << "\nInorder prolazak kroz stablo: \n";
        printInorder(root);
      
        cout << "\nPostorder prolazak kroz stablo: \n";
        printPostorder(root);
      
        return 0;
    }

.. admonition:: Output gore navedenog programa:

    Preorder prolazak kroz stablo:
    1 2 4 5 3
    
    Inorder prolazak kroz stablo:
    4 2 5 1 3
    
    Postorder prolazak kroz stablo:
    4 5 2 3 1

Zadaci za vježbu
----------------

.. admonition:: Uputstva 

    Da bi vam se priznao dolazak potrebno je rješene zadatke predati na Merlinu,rok za predaju je 02.04.2020 23:59.Rješenja  predajete  u  obliku  .cpp  datoteke,  .cpp  datoteke  imenujte (Ime_Prezime_zd_brojZadatka.cpp).
    
    Za sva pitanja stojim vam na raspolaganju na mailu: milan.petrovic@uniri.hr

    Želim vam puno sreće i uspjeha u rješavanju zadataka.

.. admonition:: Demonstrature

    Tokom sljdećeg tjedna održati će se on-line demonstrature na kojima će se rješavati zadaci koji su do sada bili zadani uz vježbe. Predlažem da pripremite pitanja vezana uz gradivo. Link na kojem možete pratiti i vrijeme naknadno će se objaviti.
    
.. admonition:: Zadatak 1

    Kreirajte proizvoljno binarno stablo, zatim definirajte funkicju koja vraća visinu stabla.

.. admonition:: Zadatak 2

    Nadopunite prethodni zadatak tako da za zadani čvor n funkcija vraća podstablo zadanog čvora.
    Dakle vaš zadatak je za zadano binarno stablo pronaći podstablo u zadanom čvoru n i njegovu visinu.
    