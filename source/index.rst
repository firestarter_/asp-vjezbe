##############################
Algoritmi i strukture podataka
##############################

******
Vježbe
******

Uvod
=======================================

.. toctree::
   :maxdepth: 2

   uvod-asp
   korisni-linkovi
   big-o-notation


Strukture podataka
=======================================

.. toctree::
   :maxdepth: 3

   linearne-strukture-podataka
   stabla
   grafovi


Algoritmi
==========================================

.. toctree::
   :maxdepth: 2

   rekurzija
   dinamicko-programiranje
   sortiranja
   algoritmi-na-grafu


Zadaci
==========================================

.. toctree::
   :maxdepth: 2

   zadaci
   zadaci-vjezbe2

