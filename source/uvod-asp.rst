Algoritmi i strukture podataka: Vježbe
======================================

**Priprema za rad Qt Creator sučelju (Winodws)**

Ako već imate neko drugo sučelje za pisati C++ kôd, možete ga goristiti, niste obavezni koristit Qt Creator, iako je preporučljivo jer ga koristimo na vježbama u učionici.

`Qt za Windows <https://doc.qt.io/qt-5/windows.html>`_

`Qt Creator Community edition <https://www.qt.io/download-open-source?hsCtaTracking=9f6a2170-a938-42df-a8e2-a9f0b1d6cdce%7C6cb0de4f-9bb5-4778-ab02-bfb62735f3e5>`_

`Dodatna pomoć prilikom instalacije za Windows <https://www.ics.com/blog/getting-started-qt-and-qt-creator-windows>`_


„Veliki O”zapis
---------------
.. image:: https://www.cdn.geeksforgeeks.org/wp-content/uploads/mypic.png

Strukture podataka
------------------
.. image:: https://i.pinimg.com/originals/ee/9d/b9/ee9db99f2c5bd40307b5698fd48dcb14.png

.. image:: https://media.geeksforgeeks.org/wp-content/uploads/20191010170332/Untitled-Diagram-183.png