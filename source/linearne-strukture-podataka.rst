Linearne strukture podataka 
===========================
Kod struktura podataka, važno je razumjeti razliku između sučelja (interface) podatkovne strukture i njene implementacije:

- Sučelje opisuje **što** struktura podataka čini.
- Implementacija opisuje **kako** to čini.

Sučelje, nazivamo još i apstraktni tip podataka, ono definira skup operacija koje podržava ta struktura podataka i semantiku, odnosno značenje, tih operacija.
 
- Sučelje nam ne govori ništa o tome na koji način se provode operacije u toj strukturi podataka.
- Pruža samo popis podržanih operacija, zajedno s specifikacijama o tome koje vrste argumenata prihvaća svaka operacija i vrijednosti koje vraća svaka operacija

S druge strane, implementacija strukture podataka:

- Uključuje unutarnji prikaz strukture podataka kao i definicije algoritama koji implementiraju operacije podržane u strukturi podataka.  

Vektori
-------

**Koriste se za:**

- Jednostavno pohranjivanje
- Dodavanje, ali ne brisanje
- Brze pretrage po indeksima

**Ne koristite se za:**

- Umetanje / brisanje na sredini popisa
- Spremnik koji se dinamički mijenja
- Indeksiranje bez cijelog broja




Primjer u C++ std::vector
^^^^^^^^^^^^^^^^^^^^^^^^^

`Izvorni kôd (vector) <https://github.com/gibsjose/cpp-cheat-sheet/blob/master/Data%20Structures%20and%20Algorithms.md#12-vector-stdvector>`_

.. code-block:: cpp
	
	//#include <vector>

	//Stvaranje vektora v tipa int
	std::vector<int> v;

	//---------------------------------
	// Osnovne operacije
	//---------------------------------

	// Unos na početak, određeni index i kraj
	v.insert(v.begin(), value);             // početak
	v.insert(v.begin() + index, value);     // index
	v.push_back(value);                     // kraj

	// Pristup početku, određenom indexu, kraju
	int head = v.front();       // početak
	int value = v.at(index);    // index
	int tail = v.back();        // kraj

	// Veličina
	unsigned int size = v.size();

	// Iteracija
	for(std::vector<int>::iterator it = v.begin(); it != v.end(); it++) {
		std::cout << *it << std::endl;
	}

	// Uklanjanje elementa s početka, određenog indexa, kraja
	v.erase(v.begin());             // početak
	v.erase(v.begin() + index);     // index
	v.pop_back();                   // kraj

	// Očisti
	v.clear();


Liste
-----

**Koriste se za:**

- Umetanje u sredinu / početak liste
- Učinkovito sortiranje

**Ne koriste se za:**

- Direktan pristup

Sučelje liste (list interface)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Sučelje liste podržava sljedeće operacije:

- size(): vraća vrijednost n, koja označava duljinu liste
- get(i): vraća vrijednost xi.
- set(i, x): postavlja vrijednost xi jednaku x.
- add(i, x): dodaje x na loakciju i, istiskujući xi, ..., xn − 1;.
- remove(i) uklanja vrijednost xi, zamjenjujući xi + 1, ..., xn − 1. 


Primjer u C++ std::list
^^^^^^^^^^^^^^^^^^^^^^^

`Izvorni kôd (list) <https://github.com/gibsjose/cpp-cheat-sheet/blob/master/Data%20Structures%20and%20Algorithms.md#14-list-stdlist-and-stdforward_list>`_

.. code-block:: cpp
	
	//#include<list>
	// Kreiranje liste l tipa int
	std::list<int> l;
	
    //----------------------------------------------------------------------
    // Osnovne operacije nad listom
    //----------------------------------------------------------------------
	
	// Unos na početak, određeni index i kraj
    l.push_front(value);                    // početak
    l.insert(l.begin() + index, value);     // index
    l.push_back(value);                     // kraj
	// Pristup početku, određenom indexu, kraju
    int head = l.front();                                           // početak
    int value = std::next(l.begin(), index);                // index
    int tail = l.back();                                            // kraj
	
	// Duljina liste
    unsigned int size = l.size();
	
	// Iteracija kroz listu
    for(std::list<int>::iterator it = l.begin(); it != l.end(); it++) {
        std::cout << *it << std::endl;
    }
	
	// Uklanjanje elementa s početka, određenog indexa, kraja
    l.pop_front();                  // početak
    l.erase(l.begin() + index);     // index
    l.pop_back();                   // kraj
	
	// Očisti
    l.clear();

    //----------------------------------------------------------------------
    // Specifične operacije nad spremnikom
    //----------------------------------------------------------------------
	
	// Ukloni element s određenom vrijednosti n
    l.remove(n);
	
	// Ukloni duple elemente
    l.unique();
	
	// Spajanje sortiranih lista l i list2 u jednu listu
    l.merge(list2);
	
	// Sortira elemente liste
    l.sort();
	
	// Sortiranje elemenata liste u obrnutom redosljedu
    l.reverse();
        


Polje (Array)
------------------

- std::array je spremnik koji obuhvaća nizove fiksne veličine.

- Polje je zbirka stavki pohranjenih na neprekidnim memorijskim mjestima, a elementima se može nasumično pristupiti koristeći indekse matrice.

**Koristi se za:**

- Pohranjivanje slične vrste elemenata jer u vrsti podataka moraju biti isti za sve elemente. 

- Normalne varijable (v1, v2, v3, ..) možemo koristiti kada imamo mali broj objekata, ali ako želimo pohraniti veliki broj instanci, postaje nam teško upravljati s normalnim varijablama. 

- Predstavlja velik broj istanci u jednoj varijabli.

- Za spremanje zbirke primitivnih vrsta podataka kao što su int, float, double, char, itd. Bilo koje određene vrste. 

- **Invalidacija iteratora**: U pravilu, iteratori za polje nikad se ne mjenjaju tokom cijelog životnog vijeka polja. Treba, međutim, uzeti u obzir da će tijekom promjene (swap) iterator nastaviti upućivati ​​na isti element polja i na taj način samo mijenja vrijednost na koju pokazuje.

Predonsti i nedostaci polja 
^^^^^^^^^^^^^^^^^^^^^^^^^^^

**Prednosti polja u C / C ++:**

- Nasumičan pristup elementima pomoću indeksa polja.
- Jednostavan pristup svim elementima.
- Prolazak kroz polje izvodi se pomoću jedne petlje.
- Sortiranje postaje jednostavno jer se može postići pisanjem manje redaka koda

**Nedostaci polja u C / C ++:**

- Omogućuje unos određenog broja elemenata o čemu se odlučuje u trenutku deklaracije. Za razliku od povezane liste.
- Umetanje i brisanje elemenata može biti memorijski zahtjevno jer je elementima potrebno upravljati.

Implementacija u C++ (std::array)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: cpp
	
	//#include<array>
	//Stvaranje polja arr tipa int, veličine 3
	std::array<int, 3> arr = {1, 2, 3};  
	
	//----------------------------------------------------------------------
	// Iteratori
	//----------------------------------------------------------------------
	
	//Početak
	arr.begin();
	//Kraj
	arr.end();
	
	//Ispis elemenata polja for petljom pomoću iteratora
	for ( auto it = arr.begin(); it != arr.end(); ++it )
	std::cout << ' ' << *it;
	
	//----------------------------------------------------------------------
	// Sadržaj 
	//----------------------------------------------------------------------
	
	//Veličina 
	arr.size();
	
	//Najveća moguća veličina (zadana)
	arr.max_size();
	
	//Provjera ako je polje prazno
	arr.empty();
	
	//----------------------------------------------------------------------
	// Pristup elementima 
	//----------------------------------------------------------------------
	
	//Pristup elementu na lokaciji i
	arr[i];
	
	//Pristup elementu na lokaciji i pomoću funkcije at()
	arr.at(i);
	
	//Pristup prvom elementu 
	arr.first();
	
	//Pristup posljednjem elementu
	arr.back();
	
	// Sortiranje elemenata polja
	std::sort(arr.begin(), arr.end());
	 
	    

Red (Queue)
-----------

`Animacija rada reda implementacijom polja <https://www.cs.usfca.edu/~galles/visualization/QueueArray.html>`_

`Animacija rada reda implementacijom liste <https://www.cs.usfca.edu/~galles/visualization/QueueLL.html>`_

**Koristiti za**

- First-In First-Out operacije
- Na primjer: Jednostavan internetski sustav za naručivanje (koji prvi dođe prvi je poslužen)

Sučelje **Red (queue)** predstavlja skup elemenata kojima možemo dodavati elemente i ukloniti sljedeći element. Točnije, operacije koje podržava sučelje za Red su:

- add(x): dodavanje vrijednosti x u red.
- remove(): uklanja sljedeću (prethodno dodanu) vrijednost, y, iz reda čekanja i vraća y.

Imajte na umu da operacija remove() nema argument. Pravila za rad sa redom odlučuje koji element se uklanja. Postoji mnogo mogućih pravila za način uklanjnanja iz reda, najčešće FIFO, prioritet i LIFO.

Implementacija u C++ (std::queue)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

`Izvorni kôd (queue) <https://github.com/gibsjose/cpp-cheat-sheet/blob/master/Data%20Structures%20and%20Algorithms.md#18-queue-stdqueue>`_

.. code-block:: cpp

	//#include<queue>
	//Stvaranje praznog reda q tipa int
	std::queue<int> q;
    
	//---------------------------------
	// Osnovne operacije
	//---------------------------------
	
	// Unos
	q.push(value);
	
	// Pristup početku, kraju
	int head = q.front();       // glava
	int tail = q.back();        // rep
	
	// Veličina
	unsigned int size = q.size();
	
	// Ukloni
	q.pop();


Prioritetni red (Priority Queue)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- uvijek uklanja najmanji element iz reda.
- remove() operacija s prioritetnim redom obično se u drugim tekstovima naziva deleteMin().

Implementacija u C++ (std::priority_queue)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
`Izvorni kôd (priority_queue) <https://github.com/gibsjose/cpp-cheat-sheet/blob/master/Data%20Structures%20and%20Algorithms.md#19-priority-queue-stdpriority_queue>`_


.. code-block:: cpp

	//include<priority_queue>
	//Stvaranje praznog prioritetnog reda p
	std::priority_queue<int> p;

	//---------------------------------
	// Osnovne operacije
	//---------------------------------

	// Unos
	p.push(value);

	// Pristup posljednjem elementu
	int top = p.top();  // 'Top' element

	// Veličina
	unsigned int size = p.size();

	// Ukloni
	p.pop();

Deque 
^^^^^

Generalizirani oblik FIFO reda i LIFO reda (Stack).

- Deque predstavlja slijed elemenata, kojima se može pristupiti s prednje i stražnje strane.
- Elementi se mogu dodavati početak i kraj niza.

Imena operacija sama objašnjavaju svoju funkciju:

- addFirst(x), removeFirst (), addLast (x) i removeLast().
- Vrijedno je napomenuti da se Stack može implementirati koristeći samo addFirst(x) i removeFirst(), dok se FIFO red može implementirati pomoću addLast(x) i removeFirst()

Implementacija u C++ (std::dequeue)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
`Izvorni kôd (deque) <https://github.com/gibsjose/cpp-cheat-sheet/blob/master/Data%20Structures%20and%20Algorithms.md#19-priority-queue-stdpriority_queue>`_


.. code-block:: cpp

	//#incude<deque>
	//Stvaranje praznog dequea tipa int
	std::deque<int> d;

	//---------------------------------
	// Osnovne operacije
	//---------------------------------

	// Unos na početak, na određeni index, kraj
	d.push_front(value);                    // glava
	d.insert(d.begin() + index, value);     // index
	d.push_back(value);                     // rep

	// Access head, index, tail
	int head = d.front();       // početak
	int value = d.at(index);    // index
	int tail = d.back();        // kraj

	// Veličina
	unsigned int size = d.size();

	// Iterator
	for(std::deque<int>::iterator it = d.begin(); it != d.end(); it++) {
		std::cout << *it << std::endl;
	}

	// Uklanjanje prvog, na određenom indexu, posljednjeg elementa
	d.pop_front();                  // glava
	d.erase(d.begin() + index);     // index
	d.pop_back();                   // rep

	// Čišćenje elemenata
	d.clear();


Set
---

**Koristi se za:**

- Uklanjanje duplih elemenata
- Uređenu dinamičku pohranu

**Ne koristi se za:**

- Jednostavno pohranjivanje
- Izravni pristup indeksom


Implementacija u C++ (std::set)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

`Izvorni kôd (set) <https://github.com/gibsjose/cpp-cheat-sheet/blob/master/Data%20Structures%20and%20Algorithms.md#16-set-stdset>`_

.. code-block:: cpp

	//#include <set>
	//Stvaranje seta s tipa int
	std::set<int> s;

	//---------------------------------
	// Osnovne operacije
	//---------------------------------

	// Unos
	s.insert(20);

	// Veličina
	int size = s.size();

	// Iterator
	for(std::set<int>::iterator it = s.begin(); it != s.end(); it++) 
	{
		std::cout << *it << std::endl;
	}

	// Ukloni
	s.erase(20);

	// Očisti sadržaj
	s.clear();

	//-------------------------------------
	// Specifične operacije nad spremnikom
	//-------------------------------------

	// Provjera ako element n postoji u setu s, vraća bool vrijednost
	bool exists = (s.find(20) != s.end());

	// Funkcija koja vraća koliko se puta element n pojavljuje u setu s
	int count = s.count(n);
	


Stog (stack)
------------
**Koristi se za:**

- First-In Last-Out operacije.
- Pristup elementima u obrnutom redosljedu.

**Stog**

- Čest način rada sa LIFO redom. U LIFO redu, zadnji dodani element je sljedeći uklonjeni.
- U radu sa Stogom imena add(x) i remove() mijenjaju se u push(x) i pop(). Ovim način izbjegava se mješanje naziva za rad sa LIFO i FIFO redovima.
- Razlika između reda i stoga: Stog koristi LIFO (Last-In First-Out) metodu, dok red koristi FIFO (First in first out) metodu za pristup i dodavanje elemenata.

Implementacija u C++ (std::stack)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
`Izvorni kôd (stack) <https://github.com/gibsjose/cpp-cheat-sheet/blob/master/Data%20Structures%20and%20Algorithms.md#17-stack-stdstack>`_


.. code-block:: cpp
	
	#include<stack>
	//Stvaranje stoga s tipa int
	std::stack<int> s;

	//-------------------------------------
	// Specifične operacije nad spremnikom
	//-------------------------------------

	// Dodavanje elementa na posljednje mjesto 
	s.push(20);

	// Veličina
	unsigned int size = s.size();

	// Brisanje elementa na posljednjem mjestu
	s.pop();

	// Pristup zadnjem elementu
	int top = s.top();





