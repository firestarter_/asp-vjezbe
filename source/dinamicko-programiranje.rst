Dinamičko programiranje
=======================

Uvod
----
Dinamičko programiranje (DP) je tehnika rješavanja posebnih problema u polinomnom vremenu.
Najčešće je optimizacija obične rekurzije. Gdje god vidimo rekurzivno rješenje koje ima ponavljane pozive za iste ulaze, možemo ga optimizirati pomoću dinamičkog programiranja.

Ideja dinamičkog programiranja je da se rješenja pojedinih problema koja tvore ukupno rješenje problema pohranjuju, kako ih kasnije ne bismo morali ponovno izračunavati. Ova jednostavna optimizacija smanjuje vremensku složenost.

.. admonition:: Na primjer

    Jednostavno rekurzivnim rješenjem za Fibonaccijeve brojeve, dobit ćemo eksponencijalnu vremensku složenost i ako je optimiziramo pohranjivanjem rješenja pojedinih problema, vremenska se složenost smanjuje na linearnu.

Dinamičko programiranje uglavnom se koristi kada su rješenja istih pojedinih problema potrebna iznova i iznova. U dinamičnom programiranju računalna rješenja pojedinih problema pohranjuju se tako da ih ne treba ponovno izračunavati. Dakle, dinamičko programiranje nije korisno kada nema uobičajenih (preklapajućih) pojedinih problema jer nema smisla pohranjivati ​​rješenja ako ona nisu ponovno potrebna.

Dinamičko programiranje je algoritamska paradigma koja rješava zadani složeni problem razbijanjem ga na pojedinačne probleme i pohranjuje rezultate kako bi se izbjeglo ponovno izračunavanje istih rezultata.

Slijede dva glavna svojstva koja sugeriraju da se zadani problem može riješiti pomoću dinamičkog programiranja:

- Svojstvo preklapanja pojedinačnih problema: Problem ima preklapanje pojedinačnih problema ako se problem može raščlaniti na pojedinačne probleme koji se više puta ponovo upotrebljavaju ili rekurzivni algoritam za problem rješava isti pojedini problem iznova i iznova, umjesto da uvijek stvara nove pojedinačne probleme.
- Svojstvo optimalnih podstruktura: Dani problemi imaju svojstvo optimalne potkonstrukcije ako se optimalno rješenje zadanog problema može dobiti korištenjem optimalnih rješenja njegovih pojedinačnih problema.


Kako rješavati probleme dinamičkim programiranjem?
---------------------------------------------------

Koraci za rješenje DP-a:

- Utvrdite je li problem DP-a

- Određivanje izraza za stanja s najmanje parametara

- Formuliranje odnosa stanja

- Pohrana rješenja pojedinačnih problema


1. korak: Kako klasificirati problem kao problem dinamičkog programiranja?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Obično se svi problemi koji zahtijevaju maksimizaciju ili minimizaciju određene količine ili problemi s prebrojavanjem načina pod određenim uvjetima ili određeni problemi vjerojatnosti mogu riješiti korištenjem dinamičkog programiranja.

Svi problemi dinamičkog programiranja zadovoljavaju svojstvo pojedinačnih problema koji se preklapaju, a većina klasičnih dinamičkih problema zadovoljava svojstvo optimalne podstrukture. Kada se uoče ova svojstva u određenom problemu, on se može riješiti pomoću DP-a.


2. korak: Odlučivanje o stanju
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Problemi s DP-om odnose se na stanje i njihove prelaze. Ovo je najosnovniji korak koji se mora učiniti vrlo pažljivo, jer prijelaz stanja ovisi o odabiru definicije stanja koju napravite. Pa, pogledajmo što podrazumijevamo pod pojmom "stanja".

Stanje se može definirati kao skup parametara koji mogu jedinstveno identificirati određeni položaj ili položaj u datom problemu. Ovaj skup parametara trebao bi biti što je moguće manji kako bi se smanjio prostor stanja.

Dakle, naš prvi korak će biti odlučivanje o stanju problema nakon što utvrdimo da je naš problem, problem DP-a.

Kao što znamo DP se odnosi na izračun rezultata pojedinačnih problema za formuliranje konačnog rezultata.
Dakle, naš sljedeći korak bit će pronalaženje odnosa između prijašnjih stanja kako bi se dostiglo trenutno stanje.



3. korak: Određivanje odnosa među stanjima
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Najzahtjevniji dio za rješenje problema s DP-om i zahtijeva mnogo intuicije, promatranja i prakse. Primjer primjene pristupa dan je u nastavku.

**Za zadana 3 broja {1, 3, 5}, moramo odrediti ukupni broj načina na koji možemo oblikovati broj 'N' koristeći zbroj danih triju brojeva.** (dopuštajući ponavljanja i različite argumente).

.. code-block::

    Ukupni broj načina stvaranja broja 6 je: 8

    1+1+1+1+1+1
    1+1+1+3
    1+1+3+1
    1+3+1+1
    3+1+1+1
    3+3
    1+5
    5+1


Razmislimo dinamički o ovom problemu. Dakle, prije svega mi odlučujemo stanja za zadani problem. Uzet ćemo parametar n da bismo odredili stanje jer može jedinstveno identificirati bilo koji pojedini problem. Dakle, naše stanje DP-a bit će stanje(n). Ovdje stanje(n) znači ukupni broj načina koji tvore n upotrebom {1, 3, 5} kao elemenata.

Sada moramo izračunati stanje(n).

**Kako to učiniti?**
Kako možemo upotrijebiti samo 1, 3 ili 5 za tvorbu zadanog broja. Pretpostavimo da znamo rezultat za n = 1,2,3,4,5,6;, možemo reći da znamo rezultat za stanje (n = 1), stanje (n = 2), stanje (n = 3) ……… stanje (n = 6)

Sada želimo odrediti rezultat stanja (n = 7). Pogledajmo na koje sve načine možemo dobiti 7 dodavanjem samo 1, 3 i 5. Rješenje za 7 možemo dobiti na sljedeća 3 načina:

**1) Dodavanje broja 1 sve kombinacije stanja (n = 6)**

.. code-block::

    [(1 + 1 + 1 + 1 + 1 + 1) + 1]
    [(1 + 1 + 1 + 3) + 1]
    [(1 + 1 + 3 + 1) + 1]
    [(1 + 3 + 1 + 1) + 1]
    [(3 + 1 + 1 + 1) + 1]
    [(3 + 3) + 1]
    [(1 + 5) + 1]
    [(5 + 1) + 1]

**2) dodavanje broja 3 u sve kombinacije stanja (n = 4)**

.. code-block::

    [(1 + 1 + 1 + 1) + 3]
    [(1 + 3) + 3]
    [(3 + 1) + 3]

**3) Dodavanje broja 5 u sve kombinacije stanja (n = 2)**

.. code-block::

    [(1 + 1) + 5]

Dobro proučite gore navedena tri slučaja koja obuhvaćaju sve moguće načine da se dobije broj 7.Iz navedenog primjera možemo reći stanje(7) = stanje(6) + stanje(4) + stanje(2) ili stanje(7) = stanje(7-1) + stanje(7-3) + stanje(7-5).

Iz čega možemo izvesti opću formulu za izračun stanja **n** koja izgleda: 
**stanje(n) = stanje (n-1) + stanje (n-3) + stanje (n-5)**

Dakle, naš će kod izgledati:

.. code-block:: cpp

    int solve(int n)
    {
       // base case
       if (n < 0)
          return 0;
       if (n == 0)
          return 1;
      
       return solve(n-1) + solve(n-3) + solve(n-5);
    }

4. korak: Pohrana rješenja u memoriju
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
    Ovo je najjednostavniji dio DP-a. Trebamo samo pohraniti odgovor o stanju da bismo ga sljedeći put kad je zatraženo mogli izravno koristiti iz svoje memorije.

Dodavanju pohrane u memoriju gore navedenom kodu:

.. code-block:: cpp

    // inicijalizacija na -1
    int dp[MAXN];
      
    int solve(int n)
    {
      if (n < 0)
          return 0;
      if (n == 0)
          return 1;
      
      // provjera ako je dani n vec izracunat
      if (dp[n]!=-1)
          return dp[n];
      
      // pohrana rezultata i vraćanje vrijednosti
      return dp[n] = solve(n-1) + solve(n-3) + solve(n-5);
    }




Primjeri
--------
Fibonaccijevi brojevi
^^^^^^^^^^^^^^^^^^^^^

Fibonaccijevi brojevi su brojevi u sljedećem cjelobrojnom nizu. (0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, ...)

- Matematički gledano, slijed Fn Fibonaccijevih brojeva definiran je rekurzivnom formulom: **Fn = Fn-1 + Fn-2**

- sa zadanim vrijednostima: **F0 = 0** i **F1 = 1.**

.. admonition:: Primjer rada programa:

    Input n: 6

    Out: 0 1 1 2 3 5



**Primjer rekurzivnog rješenja u C++**

.. code-block:: cpp

    #include <iostream>
    using namespace std;
    int fib(int x) {
        if ((x == 1) || (x == 0)) {
            return(x);
        }
        else {
            return(fib(x - 1) + fib(x - 2));
        }
    }
    int main() {
        int x, i = 0;
        cout << "Unesite n : ";
        cin >> x;
        cout << "\nFibonnacijevi brojevi : ";
        while (i < x) {
            std::cout << " " << fib(i);
            i++;
        }
        return 0;
    }

**Primjer rješenja dinamičkim programiranjem u C++**

.. code-block:: cpp

    #include <iostream>
    #include <cmath>
    using namespace std;
    
    void Fibonacci(int n)
    {
        int* fib = new int[n];
        for(int i=2; i<n; i++)
        {
            fib[0]=0;
            fib[1]=1;
            fib[i]=fib[i-1]+fib[i-2]; //pohrana izracunate vrijednosti
        }
    
            // ispis
            for(int i = 0; i < n; i++) cout << fib[i] << ' ';
    
            delete[] fib;
    }
    
    int main()
    {
        int n = 20;
        Fibonacci(n);
        return 0;
    }

.. admonition:: Vježba

    Usporedite brzinu izvođenja izračuna za prvih 50 Fibonaccijevi brojeva korištenjem rekurzije i dinamičkog programiranja.



Problem naprtnjače (Knapsack problem)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Za danu težinu i vrijednosti n predmeta, stavite ih u ruksak kapaciteta W da biste dobili maksimalnu ukupnu vrijednost u ruksaku. Drugim riječima, zadana su dva cjelobrojna niza vrijednost[] i težina[] koji predstavljaju vrijednosti i težine povezane s n stavki. Također s obzirom na cijeli broj W koji predstavlja kapacitet ruksaka, pronađite maksimalni podskup vrijednost[] tako da je zbroj težina ovog podskupina manji ili jednak W. Ne možete rastaviti predmet na manje podskupove, stavka može (1) ili ne mora (0) biti odabrana (0-1 svojstvo).

**Prikaz problema**

.. code-block:: 

    vrijednost[] = {60, 100, 120};
    težina[] = {10, 20, 30};
    W = 50;
    
    Težina=10; Vrijednost=60;
    Težina=20; Vrijednost=100;
    Težina=30; Vrijednost=120;
    Težina=(20+10); Vrijednost=(100+60);
    Težina=(30+10); Vrijednost=(120+60);
    Težina=(30+20); Vrijednost=(120+100);
    Težina=(30+20+10) > 50; //Težina je veća od zadanog kapacitatea W koji iznosi 50
    
    Rješenje: 220

**Primjer rješenja u C++**

.. code-block:: cpp

    #include<stdio.h>
    
    // Pomoćna funkcija koja vraća najveći od dva cijela broja
    int max(int a, int b) { return (a > b)? a : b; }
    
    // Vraća maksimalnu vrijednost koja se može staviti u ruksak kapaciteta W
    int knapSack(int W, int wt[], int val[], int n)
    {
    int i, w;
    int K[n+1][W+1];
    
    for (i = 0; i <= n; i++)
    {
    	for (w = 0; w <= W; w++)
    	{
    		if (i==0 || w==0)
    			K[i][w] = 0;
    		else if (wt[i-1] <= w)
    				K[i][w] = max(val[i-1] + K[i-1][w-wt[i-1]], K[i-1][w]);
    		else
    				K[i][w] = K[i-1][w];
    	}
    }
    
    return K[n][W];
    }
    
    int main()
    {
    	int val[] = {60, 100, 120};
    	int wt[] = {10, 20, 30};
    	int W = 50;
    	int n = sizeof(val)/sizeof(val[0]); //broj predmeta
    	cout << knapSack(W, wt, val, n);
    	return 0;
    } 


Zadaci za vježbu
----------------

Zadatak 1: Zbroj podskupa danog skupa
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Za dani skup ne-negativnih cijelih brojeva i vrijednosti zbroja, utvrdite postoji li podskup datog skupa s zbrojem jednakim zadanom zbroju.

**Primjer:**

.. code-block::

    Unos: skup[] = {3, 34, 4, 12, 5, 2}, zbroj = 9
    Izlaz: Istina -> Postoji podskup (4, 5) sa zbrojem 9.


**Pomoć pri rješavanju problema**

Neka **isSubsetSum(int set[], int n, int sum)** funkcija za pronalaženje podskupa iz **skupa[]** sa zbrojem jednakim **zbroj**. **n** je broj elemenata u **skup[]**.

Problem se može podijeliti na dva podprograma:

A) Uključite posljednji element, ponavljajte za n = n-1, zbroj = zbroj - skup[n-1]

B) Isključite zadnji element, ponavljajte za n = n-1.

Ako bilo koji od gore navedenih pojedinačnih problema vrati istina, vratite true.


Zadatak 2: Problem rudnika zlata
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Za zadani rudnik zlata dimenzija n * m gdje Svako polje ovog rudnika sadrži pozitivni cijeli broj koji je količina zlata u tonama. U početku je rudar u prvom stupcu, ali može biti u bilo kojem redu. On se može kretati samo (udesno ->, desno gore /, desno dolje \), iz određene pozicije, rudar se može kretati u ćeliji dijagonalno prema desno ili udesno ili dijagonalno prema dolje. Saznajte maksimalnu količinu zlata koju može prikupiti.

**Primjer**

.. code-block::

    Input : mat[][] = {{1, 3, 3},
                       {2, 1, 4},
                      {0, 6, 4}};
    Output : 12
    {(1,0)->(2,1)->(2,2)}
    
    Input: mat[][] = { {1, 3, 1, 5},
                       {2, 2, 4, 1},
                       {5, 0, 2, 3},
                       {0, 6, 1, 2}};
    Output : 16
    (2,0) -> (1,1) -> (1,2) -> (0,3) OR
    (2,0) -> (3,1) -> (2,2) -> (2,3)
    
    Input : mat[][] = {{10, 33, 13, 15},
                      {22, 21, 04, 1},
                      {5, 0, 2, 3},
                      {0, 6, 14, 2}};
    Output : 83

**Pomoć pri rješavanju**

Iz zadatka, možemo primijetiti sljedeće:

- Količina zlata je pozitivna, stoga bismo željeli pokriti maksimalnih vrijednosti sa danim ograničenjima.

- U svakom potezu krećemo se jednim korakom prema desnoj strani. Tako da uvijek završimo u posljednjem stupcu. Ako smo u posljednjen stupcu, više se ne možemo kretati desno.
