Rekurzija
==========
.. image:: https://1.bp.blogspot.com/-iMzXTc-c0ig/Ugdd4PRWIrI/AAAAAAAAANw/mpRv4-MexfQ/s1600/recursion.jpg
    :align: center
    :width: 400px
    :height: 270px
    :alt: recursion_image

Uvod
----
Izvor: https://hr.wikipedia.org/wiki/Rekurzija

U matematici i računarstvu, rekurzija specificira (ili konstruira) klasu objekata ili metoda (ili objekata iz određene klase) definiranjem nekoliko jednostavnih osnovnih slučajeva ili metoda (često samo jednu), i potom definiranjem pravila za razbijanje složenih slučajeva u jednostavnije.

Na primjer, sljedeće je rekurzivna definicija predaka osobe:

- Nečiji roditelji su njegovi pretci (osnovni slučaj);
- Roditelji bilo kojeg pretka su također pretci osobe koju promatramo (korak rekurzije).

Zgodno je zamisliti da rekurzivna definicija definira objekte u terminima "prethodno definiranih" objekata definirajuće klase.

Definicije poput ove su česte u matematici. Primjerice, formalna definicija prirodnih brojeva u teoriji skupova jest: 1 je prirodni broj, i svaki prirodni broj ima sljedbenika koji je također prirodni broj.
Drugi poznati primjer rekurzije u matematici su Fibonaccijevi brojevi. 

Primjer (Fibonaccijevi brojevi)
------------------------------------
Fibonaccijevi brojevi su brojevi u sljedećem cjelobrojnom nizu. (0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, ...)

- Matematički gledano, slijed Fn Fibonaccijevih brojeva definiran je rekurzivnom formulom: **Fn = Fn-1 + Fn-2**

- sa zadanim vrijednostima: **F0 = 0** i **F1 = 1.**

.. admonition:: Zadatak

    Napšite program u C++ koji pomoću rekurzije, za zadani **n** izračunava prvih **n** Fibonaccijevih brojeva.
    
.. admonition:: Primjer rada programa:

    Input n: 6

    Out: 0 1 1 2 3 5



Primjer rješenja u C++ 
^^^^^^^^^^^^^^^^^^^^^^
https://www.geeksforgeeks.org/program-for-nth-fibonacci-number/

.. code-block:: cpp

    #include <iostream>
    using namespace std;
    int fib(int x) {
        if ((x == 1) || (x == 0)) {
            return(x);
        }
        else {
            return(fib(x - 1) + fib(x - 2));
        }
    }
    int main() {
        int x, i = 0;
        cout << "Unesite n : ";
        cin >> x;
        cout << "\nFibonnacijevi brojevi : ";
        while (i < x) {
            std::cout << " " << fib(i);
            i++;
        }
        return 0;
    }

Zadaci za vježbu
----------------
U matematici je faktorijel pozitivnog cijelog broja **n**, označen s **n!**, umnožak svih pozitivnih cjelobrojnih brojeva manji od ili jednak **n**:

- n! = n × (n - 1) × (n - 2) × (n - 3) × ... × 3 × 2 × 1. 

Na primjer:

- 5! = 5 × 4 × 3 × 2 × 1 = 120. 

Vrijednost 0! je 1

.. admonition:: Zadatak

    Napišite program koji izračunava faktorijel od zadanog broja **n** pomoću rekurzije.
